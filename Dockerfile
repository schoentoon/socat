FROM alpine:latest AS builder

ARG SOCAT_VERSION=1.7.4.3

# Install all dependencies required for compiling socat
RUN apk add gcc musl-dev make

# Download socat sources
RUN wget http://www.dest-unreach.org/socat/download/socat-${SOCAT_VERSION}.tar.gz \
  && tar xzf socat-${SOCAT_VERSION}.tar.gz \
  && mv /socat-${SOCAT_VERSION} /socat

# musl doesn't have getprotobynumber, so we need to disable it otherwise it won't link properly
# https://git.alpinelinux.org/aports/tree/main/socat/APKBUILD#n22
ENV sc_cv_getprotobynumber_r=2

# add cflags to get a static binary..
ENV CFLAGS="-static -O2 -s"

# Compile socat to a static binary which we can copy around
RUN cd /socat \
  && ./configure \
  && make

# Switch to the scratch image
FROM scratch

# Copy the socat static binary
COPY --from=builder /socat/socat /

# we make the final image run with the nobody uid (or at least, the nobody uid on debian and alpine)
USER 65534

# set socat as the entrypoint
ENTRYPOINT [ "/socat" ]
