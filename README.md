# docker socat

A very small docker image (~350kB) with socat, which can be used relay 1 connection to another with little overhead.

Usage is the same as [alpine-docker](https://github.com/alpine-docker/socat), just simply replace the image with `registry.gitlab.com/schoentoon/socat`. It just doesn't include all of alpine and only a static binary of socat. This does mean you won't be able to `docker exec` into running containers. Due to this, openssl and readline are not included in the image and are therefore not supported.
